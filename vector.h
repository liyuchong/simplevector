/*
 * Copyright (c) 2013, 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __SIMPLE_VECTOR_H__
#define __SIMPLE_VECTOR_H__

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <assert.h>

/* This is incremented when the API is changed in non-compatible ways. */
#define SV_VERSION_MAJOR          2

/* This is incremented when features are added to the API but it remains
 * backward-compatible. */
#define SV_VERSION_MINOR          0

/* This is incremented when a bug fix release is made that does not contain any
 * API changes. */
#define SV_VERSION_REVISION       0

/**
 *  DESCRIPTION:
 *  ------------
 *  The is a simple, high performance and portable implementation
 *  of vector in C. It supports arbitrary type of element.
 *
 *  SIMPLE EXAMPLE OF USAGE:
 *  ------------------------
 *      Declare a vector of int type, called v:
 *          VECTOR(int) v;
 *
 *      Initialize v, use an initial size of 16:
 *          SV_INIT(&v, 16, int);
 *
 *      Append data:
 *          SV_PUSH_BACK(&v, 1);
 *
 *      Get element at index i:
 *          SV_AT(&v, i);
 *
 *      Erase an element at index i:
 *          SV_ERASE_AT(&v, i);
 *
 *      Clean up:
 *          SV_RELEASE(&v);
 *
 *  Note that the functions below may need to be changed according to the
 *  platform.
 */

/**
 *  Memory allocation failed, SV will stop.
 */
#define SV_PANIC() ASSERT(0)

#ifndef MALLOC
    #define MALLOC malloc
#endif /* MALLOC */

#ifndef REALLOC
    #define REALLOC realloc
#endif /* REALLOC */

#ifndef MEMCPY
    #define MEMCPY memcpy
#endif /* MEMCPY */

#ifndef ASSERT
    #define ASSERT(e) assert(e)
#endif /* ASSERT */

#ifndef MFREE
    #define MFREE free
#endif /* MFREE */

/**
 *  Declare a vector type
 *
 *  @param __vector_type The type name of the vector
 *  @param type          The type of elements
 */
#define SV_DECLARE(__vector_type, type)                                        \
    typedef VECTOR(type) __vector_type

/**
 *  Something like std::vector<type>
 *
 *  @param type Type
 */
#define VECTOR(type)                                                           \
struct                                                                         \
{                                                                              \
    type *data;                /* data buffer pointer                       */ \
    size_t size;               /* count of data in the buffer               */ \
    size_t __elementSize;      /* assigned to sizeof(type) when initialized */ \
    size_t __allocatedLength;  /* length of memory allocated                */ \
}

/**
 *  Check the memory allocation status
 *
 *  @param __ptr_in Pointer to the allocated memory address
 */
#ifndef __SIMPLE_VECTOR_MALLOC_CHECK
#define __SIMPLE_VECTOR_MALLOC_CHECK(__ptr_in)                                 \
do {                                                                           \
    if(!(__ptr_in))                                                            \
    {                                                                          \
        fprintf(stderr, "fatal: memory allocation faild in function %s(), "    \
                        "file: %s, line: %d. stop.\n",                         \
                __FUNCTION__, __FILE__, __LINE__);                             \
        SV_PANIC();                                                            \
    }                                                                          \
} while(0)
#endif

/**
 *  Initialize a vector
 *
 *  @param __vector_in The pointer to the vector
 *  @param __init_size The initial size of the vector
 *  @param __type_name The type of elements in the vector
 */
#define SV_INIT(__vector_in, __init_size, __type_name)                         \
do {                                                                           \
    ASSERT(__vector_in);                                                       \
    size_t __size_ = __init_size;                                              \
    if(__size_ == 0) __size_ = 2;                                              \
    (__vector_in)->data =                                                      \
    (__type_name *)MALLOC(sizeof(__type_name) * (__size_));                    \
        __SIMPLE_VECTOR_MALLOC_CHECK((__vector_in)->data);                     \
                                                                               \
    (__vector_in)->__allocatedLength = (__size_);                              \
    (__vector_in)->__elementSize = sizeof(__type_name);                        \
    (__vector_in)->size = 0;                                                   \
} while(0)

/**
 *  Something like std::vector<type>::push_back()
 *
 *  @param __vector_in The pointer to the vector
 *  @param __data_in   The data to be push_backed
 */
#define SV_PUSH_BACK(__vector_in, __data_in)                                   \
do {                                                                           \
    if((__vector_in)->__allocatedLength - 1 < (__vector_in)->size)             \
    {                                                                          \
        /* double the vector size */                                           \
        (__vector_in)->__allocatedLength <<= 1;                                \
                                                                               \
        /* try to get a new larger block of memory */                          \
        void *__realloced_address_ =                                           \
            REALLOC((__vector_in)->data,                                       \
            (__vector_in)->__elementSize * (__vector_in)->__allocatedLength);  \
                                                                               \
        __SIMPLE_VECTOR_MALLOC_CHECK(__realloced_address_);                    \
                                                                               \
        /* succeed, assign the new address */                                  \
        (__vector_in)->data = __realloced_address_;                            \
    }                                                                          \
                                                                               \
    /* insert new data */                                                      \
    (__vector_in)->data[(__vector_in)->size++] = (__data_in);                  \
                                                                               \
} while(0)

/**
 *  Erase an element at a given index
 *
 *  @param __vector        The pointer to the vector
 *  @param __element_index The index of the element to be erased
 */
#define SV_ERASE_AT(__vector, __element_index)                                 \
do {                                                                           \
    /* boundary test */                                                        \
    if((__element_index > (__vector)->size - 1) || (__element_index) < 0)      \
        ASSERT(!"fatal: SIMPLE_VECTOR: out of range.");                        \
                                                                               \
    /* move data */                                                            \
    MEMCPY((__vector)->data + (__element_index),                               \
           (__vector)->data + (__element_index) + 1,                           \
           (__vector)->__elementSize *                                         \
           ((__vector)->size - (__element_index) - 1));                        \
    (__vector)->size--;                                                        \
                                                                               \
    /* test if the allocated memory length needs to be shortened */            \
    __SV_CHECK_SHORTEN(__vector);                                              \
} while(0)

/**
 *  An internal use function, 
 *  tests if the allocated memory length needs to be shortened
 *
 *  @param __vector The pointer to the vector
 */
#define __SV_CHECK_SHORTEN(__vector)                                           \
do {                                                                           \
    /* test if the allocated memory is too large for the current storage */    \
    if((__vector)->size <= (__vector)->__allocatedLength >> 1)                 \
    {                                                                          \
        /* shorten the allocated length by half */                             \
        (__vector)->__allocatedLength >>= 1;                                   \
        /* The minimum allocated length is 2 */                                \
        (__vector)->__allocatedLength = (__vector)->__allocatedLength < 2 ?    \
            2 : (__vector)->__allocatedLength;                                 \
                                                                               \
        /* try to re-allocate the new memory using REALLOC() */                \
        void *__realloced_address_ =                                           \
            REALLOC((__vector)->data, (__vector)->__allocatedLength);          \
        __SIMPLE_VECTOR_MALLOC_CHECK(__realloced_address_);                    \
                                                                               \
        /* re-allocation succeed, assign the new address */                    \
        (__vector)->data = __realloced_address_;                               \
                                                                               \
        /* printf("new size: %zu, %zu elements\n",                          */ \
        /*        (__vector)->__allocatedLength, (__vector)->size);         */ \
    }                                                                          \
} while(0)

/**
 *  Get the element at a given index
 *
 *  @param __vector_in     The pointer to the vector
 *  @param __element_index The indext of element
 *
 *  @return The element at index
 */
#define SV_AT(__vertor_in, __element_index)                         \
    ((__vertor_in)->data[__element_index])

/**
 *  Get the number of elements inside the given vector
 *
 *  @param __vector_in The pointer to the vector
 *
 *  @return The count of elements
 */
#define SV_SIZE(__vector_in)                                        \
    ((__vector_in)->size)

/**
 *  Get the last element of the given vector
 *
 *  @param __vector_in The pointer to the vector
 *
 *  @return The last element
 */
#define SV_LAST_ELEMENT(__vector_in)                                \
    ((__vector_in)->data[(__vector_in)->size - 1])

/**
 *  Get the first element of the given vector
 *
 *  @param __vector_in The pointer to the vector
 *
 *  @return The first element
 */
#define SV_FIRST_ELEMENT(__vector_in)                               \
    ((__vector_in)->data[0])

/**
 *  Release the memory allocated to a vector
 *
 *  @param __vector_in The pointer to the vector
 */
#define SV_RELEASE(__vector_in)     \
do {                                \
    ASSERT(__vector_in);            \
    ASSERT((__vector_in)->data);    \
    MFREE((__vector_in)->data);     \
} while(0)

#endif /* __SIMPLE_VECTOR_H__ */
